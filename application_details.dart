void main(List<String> args) {
  var applicationDetails = new ApplicationDetails(
      appName: 'PhraZApp',
      sector: 'Enterprise Solution',
      developerName: 'Tshepiso',
      year: 2012);

  print('Below are the details of the MTN Business App of the year!!!');
  print('===============================================================');

  print(
      'In ${applicationDetails.year} The ${applicationDetails.appName} application');
  print('was selected as the best MTN Business App of the year');
  print(
      'under the ${applicationDetails.sector} category, and the name of the developer');
  print(
      'developed ${applicationDetails.appName} application is ${applicationDetails.developerName}');
  print('');
  print('========================================================');
  print('The App below is transformed from normal casing into upper case!!!');
  applicationDetails.convertToUpperCase('${applicationDetails.appName}');
}

class ApplicationDetails {
  String? appName;
  String? sector;
  String? developerName;
  int? year;

  ApplicationDetails(
      {required this.appName,
      required this.sector,
      required this.developerName,
      required this.year});

  void convertToUpperCase(String theName) {
    String name = theName;
    print('* ${name} ----> ${name.toUpperCase()}');
  }
}
