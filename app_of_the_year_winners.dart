void main(List<String> args) {
  List<MobileApplications> winners = [
    MobileApplications(name: "FNB Banking     ", year: 2012),
    MobileApplications(name: "Bookly          ", year: 2013),
    MobileApplications(name: "Live-inspect    ", year: 2014),
    MobileApplications(name: "CPUT-mobile     ", year: 2015),
    MobileApplications(name: "Domestly        ", year: 2016),
    MobileApplications(name: "OrderIn         ", year: 2017),
    MobileApplications(name: "Cowa-bunga      ", year: 2018),
    MobileApplications(name: "Digger          ", year: 2019),
    MobileApplications(name: "Checkers Sixty  ", year: 2020),
    MobileApplications(name: "Ambani Afrika   ", year: 2021)
  ];

  Comparator<MobileApplications> sortByName =
      (a, b) => a.name.compareTo(b.name);
  winners.sort(sortByName);

  print(
      "Module 02 Assignment 1.2. List of MTN Business Application of the Year...");
  print(
      "=====================================================================");
  print("List of all Application sorted by Name!!!");
  print("Applications");
  winners.forEach((MobileApplications winner) {
    print('* ${winner.name} \t ${winner.year}');
  });

  Comparator<MobileApplications> sortByYear =
      (a, b) => a.year.compareTo(b.year);
  winners.sort(sortByYear);
  print(
      "=====================================================================");
  print("The winning Mobile Application in 2017 and 2018 are as follows!!!");
  print("Application \t\t Year");
  winners.forEach((MobileApplications pastWinners) {
    if (pastWinners.year == 2017 || pastWinners.year == 2018) {
      print('* ${pastWinners.name} \t ${pastWinners.year}');
    }
  });

  print(
      "=====================================================================");
  print(
      "* The total number of mobile application since 2012 is: ${winners.length}");
}

class MobileApplications {
  String name;
  int year;

  MobileApplications({required this.name, required this.year});
}
